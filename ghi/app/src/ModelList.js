import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ModelList(props) {
    const [models, setModels] = useState([])
    const navigate = useNavigate();
    async function loadModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            console.log('Data:', data);
            setModels(data.models)
        } else {
            console.log(response);
        }
    }

    useEffect(() => {
        loadModels();
    }, [])

    return (
        <div className="container mt-4">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h2>Model List</h2>
                <button className="btn btn-primary" onClick={() => navigate('/models/create')}>
                    Add Model
                </button>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td><img src={model.picture_url} alt={model.name} style={{ width: '100px', height: 'auto' }} /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ModelList;

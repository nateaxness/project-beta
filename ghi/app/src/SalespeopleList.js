import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const SalespersonList = () => {
  const [salespeople, setSalespeople] = useState([]);
  const navigate = useNavigate();

  async function loadSalespeople() {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching salespeople:', error);
    }
  }

  const handleDelete = async (salespersonId) => {
    try {
      const response = await fetch(`http://localhost:8090/api/salespeople/${salespersonId}/`, {
        method: 'DELETE',
      });
      if (response.ok) {
        loadSalespeople();
      } else {
        console.error('Failed to delete the salesperson.');
      }
    } catch (error) {
      console.error('Error occurred while deleting the salesperson:', error);
    }
  };

  useEffect(() => {
    loadSalespeople();
  }, []);

  return (
    <div className="container mt-4">
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h2>Salesperson List</h2>
        <button className="btn btn-primary" onClick={() => navigate('/add-salesperson')}>
          Add Salesperson
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.id}>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              <td>{salesperson.employee_id}</td>
              <td>
                <button className="btn btn-danger" onClick={() => handleDelete(salesperson.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default SalespersonList;

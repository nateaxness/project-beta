import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const CustomerList = () => {
  const [customers, setCustomers] = useState([]);
  const navigate = useNavigate();

  async function loadCustomers() {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const data = await response.json();
        setCustomers(data);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching customers:', error);
    }
  }

  const handleDelete = async (customerId) => {
    try {
      const response = await fetch(`http://localhost:8090/api/customers/${customerId}/`, {
        method: 'DELETE',
      });
      if (response.ok) {
        loadCustomers();
      } else {
        console.error('Failed to delete the customer.');
      }
    } catch (error) {
      console.error('Error occurred while deleting the customer:', error);
    }
  };

  useEffect(() => {
    loadCustomers();
  }, []);

  return (
    <div className="container mt-4">
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h2>Customer List</h2>
        <button className="btn btn-primary" onClick={() => navigate('/add-customer')}>
          Add Customer
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {customers && customers.map((customer) => (
            <tr key={customer.id}>
              <td>{customer.first_name}</td>
              <td>{customer.last_name}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
              <td>
                <button className="btn btn-danger" onClick={() => handleDelete(customer.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default CustomerList;

from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
import json


@require_http_methods(['GET', 'POST'])
def salesperson_list_create_view(request):
    if request.method == 'GET':
        salespeople = Salesperson.objects.all()
        data = [{'id': sp.id, 'first_name': sp.first_name, 'last_name': sp.last_name, 'employee_id': sp.employee_id} for sp in salespeople]
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(
                first_name=data['first_name'],
                last_name=data['last_name'],
                employee_id=data['employee_id']
            )
            return JsonResponse({'id': salesperson.id, 'first_name': salesperson.first_name, 'last_name': salesperson.last_name, 'employee_id': salesperson.employee_id}, status=201)
        except json.JSONDecodeError:
            return HttpResponseBadRequest("Invalid JSON data")
        except KeyError:
            return HttpResponseBadRequest("Missing required fields")

@require_http_methods(['DELETE'])
def salesperson_delete_view(request, pk):
    salesperson = get_object_or_404(Salesperson, pk=pk)
    salesperson.delete()
    return JsonResponse({}, status=204)


@require_http_methods(['GET', 'POST'])
def customer_list_create_view(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        data = [{'id': c.id, 'first_name': c.first_name, 'last_name': c.last_name, 'address': c.address, 'phone_number': c.phone_number} for c in customers]
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            customer = Customer.objects.create(
                first_name=data['first_name'],
                last_name=data['last_name'],
                address=data['address'],
                phone_number=data['phone_number']
            )
            return JsonResponse({'id': customer.id, 'first_name': customer.first_name, 'last_name': customer.last_name, 'address': customer.address, 'phone_number': customer.phone_number}, status=201)
        except json.JSONDecodeError:
            return HttpResponseBadRequest("Invalid JSON data")
        except KeyError:
            return HttpResponseBadRequest("Missing required fields")

@require_http_methods(['DELETE'])
def customer_delete_view(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    customer.delete()
    return JsonResponse({}, status=204)

@require_http_methods(["GET", "POST"])
def sale_list_create_view(request):
    if request.method == 'GET':
        sales = Sale.objects.exclude(automobile__sold=False)
        data = []
        for sale in sales:
            automobile = AutomobileVO.objects.get(pk=sale.automobile_id)
            salesperson = Salesperson.objects.get(pk=sale.salesperson_id)
            customer = Customer.objects.get(pk=sale.customer_id)

            sale_data = {
                'id': sale.id,
                'automobile': {
                    'id': automobile.vin,
                    'vin': automobile.vin,
                    'sold': automobile.sold,
                },
                'salesperson': {
                    'id': salesperson.id,
                    'first_name': salesperson.first_name,
                    'last_name': salesperson.last_name,
                    'employee_id': salesperson.employee_id,
                },
                'customer': {
                    'id': customer.id,
                    'first_name': customer.first_name,
                    'last_name': customer.last_name,
                    'address': customer.address,
                    'phone_number': customer.phone_number,
                },
                'price': str(sale.price),
            }
            data.append(sale_data)

        return JsonResponse(data, safe=False)


    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            sale = Sale.objects.create(
                automobile_id=data['automobile'],
                salesperson_id=data['salesperson'],
                customer_id=data['customer'],
                price=data['price'],
            )
            automobile = AutomobileVO.objects.get(pk=data['automobile'])
            automobile.sold = True
            automobile.save()

            return JsonResponse({
                'id': sale.id,
                'automobile': sale.automobile_id,
                'salesperson': sale.salesperson_id,
                'customer': sale.customer_id,
                'price': str(sale.price)
            }, status=201)
        except json.JSONDecodeError:
            return HttpResponseBadRequest("Invalid JSON data")
        except KeyError:
            return HttpResponseBadRequest("Missing required fields")


@require_http_methods(['DELETE'])
def sale_delete_view(request, pk):
    sale = get_object_or_404(Sale, pk=pk)
    sale.delete()
    return JsonResponse({}, status=204)

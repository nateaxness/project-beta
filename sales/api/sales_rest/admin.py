from django.contrib import admin
from .models import Salesperson, Customer, Sale, AutomobileVO


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'employee_id')
    list_display_links = ('id', 'first_name', 'last_name')


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'address', 'phone_number')
    list_display_links = ('id', 'first_name', 'last_name')


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ('price', 'automobile', 'salesperson', 'customer')
    list_display_links = ('price',)
    

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ('vin', 'sold')
    list_display_links = ('vin',)
    list_filter = ('sold',)

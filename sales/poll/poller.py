import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

def automobile_data():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    if response.status_code == 200:
        content = response.json()
        for automobile in content["autos"]:
            AutomobileVO.objects.update_or_create(
                vin=automobile["vin"],
                defaults={
                    "sold": automobile["sold"],
                },
            )

def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            automobile_data()
        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(1)

if __name__ == "__main__":
    poll()

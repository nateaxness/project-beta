from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, Appointment
import json
import datetime


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name",
                  "last_name",
                  "employee_id",
                  "id"
                  ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id",
                  "reason",
                  "vin",
                  "customer",
                  "status",
                  "technician",
                  "date_time",
                  ]

    encoders = {"technician": TechnicianListEncoder()}


@require_http_methods(["GET", "POST"])
def technician_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def delete_technician(request, id):
    count, _ = get_object_or_404(Technician, id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods({"GET", "POST"})
def appointment_list(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            print(content)
            try:

                technician = Technician.objects.get(id=content["technician"])  # noqa
                print(technician)
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse({"message": "technician could not be found"}, status=400)  # noqa
            appointment = Appointment.objects.create(**content)
            return JsonResponse(appointment,
                                encoder=AppointmentListEncoder,
                                safe=False
                                )
        except Exception as error:
            return JsonResponse({"message": str(error)}, status=400)


@require_http_methods({"DELETE"})
def delete_appointment(request, id):
    count, _ = get_object_or_404(Appointment, id=id).delete()
    return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods({"PUT"})
def finish_appointment(request, id):
    status = Appointment.objects.filter(id=id).update(status="finished")
    if status == 0:
        return JsonResponse({"error": "appointment does not exist"}, status=404)  # noqa
    else:
        return JsonResponse({"success": "appointment completed"})


@require_http_methods({"PUT"})
def cancel_appointment(request, id):
    status = Appointment.objects.filter(id=id).update(status="canceled")
    if status == 0:
        return JsonResponse({"error": "appointment does not exist"}, status=404)  # noqa
    else:
        return JsonResponse({"success": "appointment canceled"})
